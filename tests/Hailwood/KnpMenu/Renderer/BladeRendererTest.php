<?php namespace Tests\Hailwood\KnpMenu\Renderer;

use Illuminate\View\Factory;
use Mockery as m;
use PHPUnit_Framework_TestCase as TestCase;
use Illuminate\View\FileViewFinder;
use Knp\Menu;
use Knp\Menu\Matcher\Matcher;
use Hailwood\KnpMenu\Renderer\BladeRenderer;

class BladeRendererTest extends TestCase
{
	public function testRender()
	{
		$env = $this->getEnvironment();
		/*$finder = $env->getFinder();
		$matcher = new Matcher();
		$renderer = new BladeRenderer($env, 'default', $matcher, array());
		$factory = new Menu\MenuFactory();
		$item = new Menu\MenuItem('root', $factory);
		$renderer->render($item, array());*/
	}

	protected function getEnvironment()
	{
		return new Factory(
			m::mock('Illuminate\View\Engines\EngineResolver'),
			m::mock('Illuminate\View\ViewFinderInterface'),
			m::mock('Illuminate\Events\Dispatcher')
		);
	}
}
