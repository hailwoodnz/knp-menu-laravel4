<?php namespace Tests\Hailwood\KnpMenu\Generators;

use Mockery as m;
use PHPUnit_Framework_TestCase as TestCase;
use Hailwood\KnpMenu\Generators\MenuGenerator;

class MenuGeneratorTest extends TestCase {

	public function tearDown()
	{
		m::close();
	}

	public function testMenuCanBeCreated()
	{
		$generator = new MenuGenerator($files = m::mock('Illuminate\Filesystem\Filesystem[put]'));
		$menu = $generator->getMenu('FooMenu');
		$files->shouldReceive('put')->once()->with(__DIR__.'/FooMenu.php', $menu);
		$generator->make('foo', __DIR__);
	}

	public function testMenuCannotBeCreated()
	{
		$generator = new MenuGenerator($files = m::mock('Illuminate\Filesystem\Filesystem[exists]'));
		$menu = $generator->getMenu('FooMenu');
		$files->shouldReceive('exists')->once()->with(__DIR__.'/FooMenu.php')->andReturn(true);
		$this->assertFalse($generator->make('foo', __DIR__));
	}
}
