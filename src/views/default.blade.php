
@if ($item->hasChildren() and 0 !== $options['depth'] and $item->getDisplayChildren())

    <ul{{ HTML::attributes($listAttributes) }}>

    @if ($options['depth']) <?php $options['depth'] -= 1 ?> @endif

    @foreach ($item->getChildren() as $children)

        <?php $attributes = $item->getAttributes(); ?>
        <?php $classes = $itemClasses = $children->getAttribute('class') ? array($children->getAttribute('class')) : array(); ?>

        @if ($matcher->isCurrent($children))
            <?php $classes[] = $options['currentClass']; ?>
        @endif

        @if ($matcher->isAncestor($children, $options['depth']))
            <?php $classes[] = $options['ancestorClass']; ?>
        @endif

        @if ($children->actsLikeFirst())
            <?php $classes[] = $options['firstClass']; ?>
        @endif

        @if ($children->actsLikeLast())
            <?php $classes[] = $options['lastClass']; ?>
        @endif

        @if (count($classes))
            <?php $attributes += array('class' => join(' ', $classes)); ?>
        @endif

        <li{{ HTML::attributes($attributes) }}>
            <?php $label = e($children->getLabel()); ?>
            @if ($options['allow_safe_labels'] and $children->getExtra('safe_label', false))
                <?php $label = $children->getLabel(); ?>
            @endif

            @if (strlen($children->getUri()) and (!$matcher->isCurrent($children) or true === $options['currentAsLink'])) {{ HTML::link($children->getUri(), $label, $children->getLinkAttributes()) }}
            @else
                <span{{ HTML::attributes($children->getLabelAttributes()) }}>{{ $label }}</span>
            @endif

            <?php
                $itemClasses = $children->getAttribute('class') ? array($children->getAttribute('class')) : array();
                $itemClasses[] = 'menu_level_'.$children->getLevel();
                $childrenAttributes = array_merge($children->getChildrenAttributes(), array('class' => join(' ', $itemClasses)));
            ?>

            @include('knp-menu::default', array('item' => $children, 'options' => $options, 'matcher' => $matcher, 'listAttributes' => $childrenAttributes))
        </li>

    @endforeach

    </ul>

@endif
