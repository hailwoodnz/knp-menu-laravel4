<?php namespace Hailwood\KnpMenu;

use Knp\Menu\MenuFactory;
use Knp\Menu\Matcher\Matcher;
use Knp\Menu\Renderer\ListRenderer;
use Symfony\Component\Finder\Finder;
use Illuminate\Support\Str;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Engines\CompilerEngine;
use Illuminate\Support\ClassLoader;

class KnpMenuServiceProvider extends ServiceProvider
{

    /**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
    protected $defer = true;

    /**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
    public function boot()
    {
        $this->package('hailwood/knp-menu');
        $this->commands('knp_menu.command.make');
    }

    /**
	 * Register the service provider.
	 *
	 * @return void
	 */
    public function register()
    {
        $path = $this->app['path'] . '/menus';

        ClassLoader::addDirectories(array( $path ));

        $this->app['knp_menu.factory'] = $this->app->share(function ($app) {
            $factory = new MenuFactory();
            if (isset( $app['url'] )) {
                    $factory->addExtension(new RoutingExtension($app['url']));
            }

            return $factory;
        });

        $this->app['knp_menu.matcher'] = $this->app->share(function ($app) {
            /** @var \Illuminate\Http\Request $request */
            $request = $app['request'];

            /** @var \Illuminate\Events\Dispatcher $events */
            $events = $app['events'];

            $voter = new Voter\RouteVoter();

            $voter->setRequest($request->instance());
            $voter->setRouter($app['router']);

            $matcher = new Matcher();
            $matcher->addVoter($voter);

            $events->fire('knp_menu.matcher.configure', array( $matcher ));

            return $matcher;
        });

        $this->app['knp_menu.menus'] = $this->app->share(function ($app) use ($path) {
            $menus = array();
            foreach ($this->app['knp_menu.finder'] as $file) {
                /** @var \FileSystemIterator $file */

                $class        = $file->getBasename('.php');
                $name         = Str::snake($file->getBasename('Menu.php'));
                $menus[$name] = $id = 'knp_menu.' . $name;
                /** @var MenuBuilder $builder */
                $builder      = new $class( $app['knp_menu.factory'] );
                $app[$id]     = $builder->create();
            }

            return $menus;
        });

        $this->app['knp_menu.renderer.list'] = $this->app->share(function ($app) {
            return new ListRenderer( $app['knp_menu.matcher'], array(), $this->config('charset') );
        });

        $this->app['knp_menu.menu_provider'] = $this->app->share(function ($app) {
            return new Provider\ContainerAwareProvider( $app, $app['knp_menu.menus'] );
        });

        $this->app['knp_menu.renderer_provider'] = $this->app->share(function ($app) {

            $hasBlade = isset( $app['knp_menu.renderer.blade'] );

            $app['knp_menu.renderers'] = array_merge(
                array( 'list' => 'knp_menu.renderer.list' ),
                $hasBlade ? array( 'blade' => 'knp_menu.renderer.blade' ) : array(),
                $this->config('renderers', array())
            );

            $default = $this->config('default_renderer', $hasBlade ? 'blade' : 'list');

            return new Renderer\ContainerAwareProvider( $app, $default, $app['knp_menu.renderers'] );
        });

        $this->app['knp_menu.helper'] = $this->app->share(function ($app) {
            return new Engines\Helper( $app['knp_menu.renderer_provider'], $app['knp_menu.menu_provider'] );
        });

        /** @var \Illuminate\View\Engines\EngineResolver $resolver */
        $resolver = $this->app['view.engine.resolver'];
        // This is seriously not classy at all but the engine resolver got no "has" method atm.
        if (@$resolver->resolve('blade') instanceof CompilerEngine) {
            $this->app['knp_menu.renderer.blade'] = $this->app->share(function ($app) {
                $view = $this->config('view');

                return new Renderer\BladeRenderer( $this->app['view'], $view, $app['knp_menu.matcher'] );
            });
        }

        $this->app['knp_menu.finder'] = $this->app->share(function ($app) use ($path) {
            $finder = new Finder();

            return $finder->files()->in($path)->name('*Menu.php');
        });

        $this->app['knp_menu.generator'] = $this->app->share(function ($app) {
            return new Generators\MenuGenerator( $app['files'] );
        });

        $this->app['knp_menu.command.make'] = $this->app->share(function ($app) use ($path) {
            return new Console\Command\MakeMenuCommand( $app['knp_menu.generator'], $path );
        });
    }

    /**
	 * Returns a specific configuration key.
	 *
	 * @param string $key
	 * @param mixed  $default
	 *
	 * @return mixed
	 */
    public function config($key, $default = null)
    {
        /** @var \Illuminate\Config\Repository $config */
        $config = $this->app['config'];

        return $config->get(sprintf('%s::config.%s', 'knp-menu', $key), $default);
    }

    /**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
    public function provides()
    {
        return array(
            'knp_menu.factory',
            'knp_menu.matcher',
            'knp_menu.menu_provider',
            'knp_menu.renderer_provider',
            'knp_menu.renderer.list',
            'knp_menu.renderer.blade',
            'knp_menu.helper',
            'knp_menu.providers',
            'knp_menu.menus',
            'knp_menu.finder',
            'knp_menu.generator',
            'knp_menu.command.make',
        );
    }
}
