<?php

use Hailwood\KnpMenu\MenuBuilder;

class {{class}} extends MenuBuilder
{
    /**
	 * Returns an instance of the menu created by the builder.
	 *
	 * @return \Knp\Menu\ItemInterface
	 */
    public function create()
    {
        $menu = $this->factory->createItem('root');

        // $menu->addChild('Home');

        return $menu;
    }
}
