<?php

namespace Hailwood\KnpMenu\Renderer;

use Knp\Menu\Renderer\RendererInterface;
use Knp\Menu\Matcher\MatcherInterface;
use Knp\Menu\ItemInterface;
use Illuminate\View\Factory;
use Illuminate\View\View;

class BladeRenderer implements RendererInterface
{
    /**
     * @var \Illuminate\View\Factory
     */
    private $environment;

    /**
     * @var \Knp\Menu\Matcher\MatcherInterface
     */
    private $matcher;

    /**
     * @var array
     */
    private $options;

    public function __construct(Factory $environment, $view, MatcherInterface $matcher, array $options = array())
    {
        $this->environment = $environment;
        $this->matcher = $matcher;
        $this->options = array_merge(array(
            'currentAsLink' => true,
            'currentClass' => 'current',
            'ancestorClass' => 'current_ancestor',
            'firstClass' => 'first',
            'lastClass' => 'last',
            'allow_safe_labels' => true,
            'clear_matcher' => true,
            'depth' => null,
            'view' => $view,
        ), $options);
    }

    public function render(ItemInterface $item, array $options = array())
    {
        if (!isset($options['view'])) {
            $options['view'] = $item->getExtra('view', $this->options['view']);
        }

        $options = array_merge($this->options, $options);
        $view = $options['view'];

        if (!$view instanceof View) {
            $path = $this->environment->getFinder()->find($view);
            $engine = $this->environment->getEngineResolver()->resolve('blade');
            $view = new View($this->environment, $engine, $view, $path, [
                'matcher' => $this->matcher,
                'options' => $options,
                'item' => $item,
                'listAttributes' => $item->getChildrenAttributes(),
            ]);
        }

        if (isset($options['clear_matcher'])) {
            $this->matcher->clear();
        }

        return $view->render();
    }
}
