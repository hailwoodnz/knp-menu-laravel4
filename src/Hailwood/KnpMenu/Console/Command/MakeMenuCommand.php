<?php namespace Hailwood\KnpMenu\Console\Command;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Hailwood\KnpMenu\Generators\MenuGenerator;

class MakeMenuCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'menu:make';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new menu.';

    /**
     * Create a new command instance.
     *
     * @param MenuGenerator $generator
     * @param string        $path
     */
    public function __construct(MenuGenerator $generator, $path)
    {
        parent::__construct();

        $this->path = $path;
        $this->generator = $generator;
    }

    /**
     * Execute the console command.
     */
    public function fire()
    {
        $this->generateMenu();
    }

    public function generateMenu()
    {
        $menu = $this->input->getArgument('name');
        $path = $this->getPath();
        $options = array();

        if (!$this->generator->make($menu, $path, $options)) {
            $this->error('Menu already exists!');
        } else {
            $this->info('Menu created successfully!');
        }
    }

    protected function getPath()
    {
        if (!is_null($this->input->getOption('path'))) {
            return $this->laravel['path.base'].'/'.$this->input->getOption('path');
        }

        return $this->path;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('name', InputArgument::REQUIRED, 'The name of the menu class'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('path', null, InputOption::VALUE_OPTIONAL, 'Where to place the menu'),
        );
    }

}
