<?php

return array(

    'view' => 'knp-menu::default',

    /*
	|--------------------------------------------------------------------------
	| Charset
	|--------------------------------------------------------------------------
	|
	| The charset to be used when rendering menus. 'UTF-8' by default.
	|
	*/
    'charset' => 'UTF-8',

    'renderers' => array(),

    'default_renderer' => 'blade'
);
